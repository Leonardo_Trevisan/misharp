package mono.android.app;

public class XamarinAndroidEnvironmentVariables
{
	// Variables are specified the in "name", "value" pairs
	public static final String[] Variables = new String[] {
		"MONO_LOG_LEVEL", "info",
		"XAMARIN_BUILD_ID", "92cbbcec-b5ad-49fb-8be0-4c10668c9a2f",
		"XA_HTTP_CLIENT_HANDLER_TYPE", "Xamarin.Android.Net.AndroidClientHandler",
		"XA_TLS_PROVIDER", "btls",
		"MONO_GC_PARAMS", "major=marksweep-conc",

	};
}
