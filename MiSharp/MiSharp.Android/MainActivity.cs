﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms;
using Android.Content;

namespace MiSharp.Droid
{
    [Activity(Label = "MiSharp", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            Matcha.BackgroundService.Droid
                .BackgroundAggregator.Init(this);

            //Inicializa serviço de bluethooth em segundo plano
            MessagingCenter.Subscribe<StartSmartwatchDataUpdateMessage>(this,
                "StartSmartwatchDataUpdateMessage", message =>
                {
                    var intent = new Intent(this, typeof(BluetoothUpdaterService));
                    StartForegroundService(intent);
                });

            MessagingCenter.Subscribe<StopSmartwatchDataUpdateMessage>(this,
                "StopSmartwatchDataUpdateMessage", message =>
                {
                    var intent = new Intent(this, typeof(BluetoothUpdaterService));
                    StopService(intent);
                });

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());

            this.RequestPermissions(new[]
            {
                "Manifest.Permission.AccessCoarseLocation",
                "Manifest.Permission.BluetoothPrivileged"
            }, 0);
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}