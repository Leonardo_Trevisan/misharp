﻿using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;

namespace MiSharp.Droid
{
    /// <summary>
    /// Essa classe implementa um comportamento a ser executado quando o aplicativo
    /// estiver em segundo plano/fechado.
    /// Ele executa o método ReadAndUpdate levando a execução pro projeto MiSharp, que 
    /// contém código compartilhado. Uma classe semelhante deve ser implementada pro
    /// projeto MiSharp.iOS.
    /// </summary>
    [Service]
    public class BluetoothUpdaterService : Service
    {
        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            Task.Run(async () =>
            {
                var updater = new SmartwatchDataUpdate();
                await updater.ReadAndUpdate();
            });
            return StartCommandResult.Sticky;
        }
    }
}