﻿using System.IO;

namespace MiSharp
{
    /// <summary>
    /// Essa classe estava sendo produzida para trabalhar com gráficos
    /// avançados. Por hora ignorar.
    /// </summary>
    public class Bitmap
    {
        private void bytedefine(int number, int index)
        {
            byte value;
            while (number > 0)
            {
                value = (byte)(number % 256);
                number -= value;
                this.data[index++] = value;
                number /= 256;
            }
        }
        private byte[] data;
        private int stride;

        public int Width { get; private set; }
        public int Height { get; private set; }

        public Bitmap(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            this.stride = 1;
            while (this.stride < 4 * this.Width)
                this.stride *= 4;
            this.data = new byte[54 + stride * this.Height];

            data[0] = (byte)'B';
            data[1] = (byte)'M';
            bytedefine(data.Length, 2); //File Size
            data[10] = 54; //Full Header Size (data offset)
            data[14] = 40; //DIB Header Size
            bytedefine(width, 18); //Width Pixels
            bytedefine(height, 22); //Heigth Pixels
            data[26] = 1; //Planes
            data[28] = 32; //bits per pixel
            //Size of Raw Data (with stride)
            bytedefine(this.stride * this.Height, 34);
            data[38] = 19; //Pixel Resolution
            data[39] = 11; //Pixel Resolution
            data[42] = 19; //Pixel Resolution
            data[43] = 11; //Pixel Resolution
        }

        public void setPixel(byte r, byte g, byte b, int x, int y)
        {
            this.data[54 + y * this.stride + 4 * x + 0] = b;
            this.data[54 + y * this.stride + 4 * x + 1] = g;
            this.data[54 + y * this.stride + 4 * x + 2] = r;
        }

        public void Save(string path)
        {
            FileStream stream;
            if (!File.Exists(path))
                stream = File.Create(path);
            else stream = File.Open(path, FileMode.Open);
            stream.Write(data, 0, data.Length);
            stream.Close();
        }
    }
}