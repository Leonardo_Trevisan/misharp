﻿using System;
using System.IO;
using Xamarin.Forms;
using System.Net.Http;
using Xamarin.Essentials;
using System.ComponentModel;
using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace MiSharp
{
    using Model;
    using Model.Devices;
    using Services;
    using System.Text;

    /// <summary>
    /// Código associado a página principal
    /// </summary>
    [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //Referência da página de conexão com smart bands
        DeviceConnectionPage page;
        //Referência ao código de interação com a MiBand3
        MiBand3 mi => page.Smartwatch as MiBand3;
        //Referência ao código de interação com a HonorBand5
        HonorBand5 honor => page.Smartwatch as HonorBand5;

        //Abre a página de conexão com smart bands
        private async void SearchDevice(object sender, EventArgs e)
        {
            page = new DeviceConnectionPage();
            await Navigation.PushAsync(page);
        }

        //Carrega dados na tela
        private async void MostrarDados(object sender, EventArgs e)
        {
            try
            {
                int steps = await honor.GetSteps();
                float bat = await honor.GetBatery();
                int rate = await honor.GetHearthFrequency();
                lbSteps.Text =
                    $"Você deu {steps} passos!.\n" +
                    $"Sua bateria esta em {bat * 100}%.\n" +
                    $"Sua frequência cardiaca é {rate}Hz.";
            }
            catch (Exception exc)
            {
                await this.DisplayAlert("Erro", exc.Message, "Ok");
            }
        }

        #region old methods

        private async void StartSession(object sender, EventArgs e)
        {
            try
            {
                await mi.Authentication();
            }
            catch (Exception exc)
            {
                await this.DisplayAlert("Erro", exc.Message, "Ok");
            }
        }

        private async void RestartLocalData(object sender, EventArgs e)
        {
            await mi.EnableRealTime();
        }

        private void CarregarImagem(object sender, EventArgs e)
        {
            string filename = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder
                .LocalApplicationData), "image.bmp");

            Bitmap bmp = new Bitmap(1024, 680);

            for (int x = 0; x < bmp.Width; x++)
            {
                bmp.setPixel(255, 0, 0, x, (int)(
                    bmp.Height / 2 + 200 * Math.Sin(x / 102.4)));
            }   

            bmp.Save(filename);

            //panel.Source = filename;
        }

        private void MostrarNotificacao(object sender, EventArgs e)
        {
            SmartwatchDataUpdate.Start();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            HealthData data = new HealthData();
            data.CustomerID = 1;
            data.HearthFrequency = await mi.GetHearthFrequency();
            data.Steps = await mi.GetSteps();
            data.Oximetry = -1;
            data.Moment = DateTime.Now;

            var client = new HttpClient();
            client.BaseAddress = new Uri(@"http://miwebsharp.gear.host");
            HttpResponseMessage response = await client.GetAsync("/api/healthdata/"
                + data.ToString());
                
            response = await client.GetAsync("/api/healthdata");
            //lb.Text = await response.Content.ReadAsStringAsync();
        }

        #endregion
    }
}