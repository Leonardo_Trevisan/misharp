﻿using System;

namespace MiSharp.Model.Language
{
    public class Portuguese : Language
    {
        public Portuguese() : base("portuguese") { }

        protected override void CreateIfNotExist(Action<string, string> addmessage)
        {
            addmessage("ERRO0001", "O Bluetooth está indisponível.");
            addmessage("ERRO0002", "O uso do Bluetooth não foi autorizado.");
            addmessage("ERRO0003", "O Bluetooth está desligado.");
            addmessage("ERRO0004", "Nenhum dispositivo foi encontrado.");
        }
    }
}