﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MiSharp.Model.Language
{
    public delegate void LanguageUpdateHandler(Language newlang);

    /// <summary>
    /// Implementação para futura possibilidade de implementar aplicativos
    /// com mais de uma linguagem. Por hora ignorar.
    /// </summary>
    public abstract class Language
    {
        public static Language Current { get; private set; }

        public static void SetLanguage(Language lang)
        {
            if (Current.OnLanguageUpdate != null)
                Current.OnLanguageUpdate(lang);
            lang.OnLanguageUpdate += Current.OnLanguageUpdate;
        }

        public static void AddLanguageUpdateEvent(LanguageUpdateHandler handler)
        {
            Current.OnLanguageUpdate += handler;
        }

        static Language()
        {
            Current = new Portuguese();
        }

        private Dictionary<string, string> messages { get; }
            = new Dictionary<string, string>();
        public Language(string name)
        {
            string filename = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder
                .LocalApplicationData), name + ".txt");
            if (!File.Exists(filename))
            {
                File.Create(filename).Close();
                StreamWriter sw = new StreamWriter(filename);
                Action<string, string> add = (code, message)
                    => sw.WriteLine(code + ":" + message);
                CreateIfNotExist(add);
                sw.Close();
            }
            StreamReader sr = new StreamReader(filename);
            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                if (line.Length < 3)
                    continue;
                var codemess = line.Split(new char[] { ':' }, 2);
                messages.Add(codemess[0], codemess[1]);
            }
        }
        public string this[string code] => messages[code];

        public event LanguageUpdateHandler OnLanguageUpdate;
        protected virtual void CreateIfNotExist(Action<string, string> addmessage)
        { }
    }
}