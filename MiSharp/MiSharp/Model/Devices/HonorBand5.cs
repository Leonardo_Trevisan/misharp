﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Security.Cryptography;
using System.Linq;

using Plugin.BLE.Abstractions.Contracts;
using static System.Diagnostics.Trace;
using Plugin.BLE.Abstractions.EventArgs;
using Plugin.BLE.Abstractions.Extensions;
using Plugin.BLE.Abstractions.Utils;

namespace MiSharp.Model.Devices
{
    /// <summary>
    /// Um retrato de sofrimento e horas gastas.
    /// Maldita seja a Huawei e sua vontade de proteger seu hardware
    /// contra nerds da computação.
    /// </summary>
    public class HonorBand5 : Smartwatch
    {
        public override string DeviceName => "HONOR Band 5-650";

        public async override Task Authentication()
        {
            byte[] response;
            response = await WriteAndWait(
                build_message(1, 1,
                              1, 0,
                              2, 0,
                              3, 0,
                              4, 0));

            byte[] tlvs = new byte[54], param;
            Random r = new Random(DateTime.Now.Millisecond);
            param = Enumerable.Repeat(0, 32)
                           .Select(n => (byte)r.Next(0, 255))
                           .ToArray();
            tlvs[0] = 1; tlvs[1] = 32;
            for (int i = 0; i < 32; i++)
                tlvs[2 + i] = param[i];
            tlvs[35] = 2; tlvs[36] = 18;
            tlvs[37] = 0; tlvs[38] = 1;
            byte[] mess = new byte[16];
            for (int i = 0; i < 16; i++)
                mess[i] = response[response.Length - 16 + i];
            mess = aesCrypt(mess, generateKey(MacAddress), new byte[16]);
            for (int i = 0; i < 16; i++)
                tlvs[i + 38] = mess[i];

            response = await WriteAndWait(
                build_message(1, 19, tlvs));

            WriteLine("\t\t\t\tTOP");
        }

        public async override Task<int> GetSteps()
        {
            var r = await WriteAndWait(build_message(
                1, 7,
                124, 1, 1,
                125, 0,
                126, 0
                ));
            byte[] data = ("aa ab cc 93 b0 2f 70 99 ce ec 61 " +
                "77 d1 ea f2 aa 0c 3a ed 32 4c 2a 6d a3 76 db " +
                "d1 c0 7a 76 4b 32 ff 3c 16 db 1e 7b 34 34 f7 " +
                "17 7f 7c 93 d8 6e ea 0c de 67 34 19 79 1a a1 " +
                "bf 7f a1 23 7f ec ff f3 82 ee e6 08 58 4a d3 " +
                "5d 05 e5 78 a7 f8 b0 a5 68 41 da 00 de 61 4f " +
                "ec 79 72 02 cb 95 41 fb e1 2c")
                .Replace(" ", "").ToHex();
            byte[] iv = ("D5 D6 AF 22 7F D1 89 FC 8A A3 82 FC 4A AA 0D 93")
                .Replace(" ", "").ToHex();
            byte[] key = generateLinkKey(MacAddress);

            var response = aesDecrypt(data, key, iv);

            return r[0];
        }

        public async override Task<float> GetBatery()
        {
            await new Task(() => { });
            return -1.0f;
        }

        public async override Task<int> GetHearthFrequency()
        {
            await new Task(() => { });
            return -1;
        }

        ICharacteristic f01 = null, f02 = null;
        private async Task<byte[]> WriteAndWait(byte[] data)
        {
            if (f01 == null)
            {
                var list = services.Select(s => s.Uuid.ToString()).ToArray();
                f01 = GetServiceByUuid("0000fe01-0000-1000-8000-00805f9b34fb");
                f02 = GetServiceByUuid("0000fe02-0000-1000-8000-00805f9b34fb");
            }
            if (f01.CanWrite)
            {
                last = null;
                f02.ValueUpdated += F02_ValueUpdated;
                while (last == null)
                {
                    await f02.StartUpdatesAsync();
                    await f01.WriteAsync(data);
                    Thread.Sleep(150);
                }
            }
            return last;
        }

        byte[] last = null, newvalue = null;
        private void F02_ValueUpdated(object sender, CharacteristicUpdatedEventArgs e)
        {
            newvalue = e.Characteristic.Value;
            WriteLine("\t\t\t" + "F02 updated!");
            string str = "[";
            for (int j = 0; j < newvalue.Length - 1; j++)
                str += " " + newvalue[j].ToString() + ",";
            str += " " + newvalue[newvalue.Length - 1].ToString() + " ]";
            WriteLine("\t\t\t" + str);
            last = newvalue;
        }

        private byte[] build_message(int service, int command, params byte[] data)
        {
            byte[] message = new byte[data.Length + 8];
            message[0] = 90;
            message[1] = (byte)((data.Length + 3) / 256);
            message[2] = (byte)((data.Length + 3) % 256);
            message[3] = 0;
            message[4] = (byte)service;
            message[5] = (byte)command;
            for (int i = 0, j = 6; i < data.Length; i++, j++)
            {
                message[j] = data[i];
            }
            message = CRC(message);

            return message;
        }

        private byte[] CRC(byte[] data)
        {
            const ushort generator = 4129;
            ushort crc = 0;

            for (int k = 0; k < data.Length - 2; k++)
            {
                crc ^= (ushort)(data[k] << 8);

                for (int i = 0; i < 8; i++)
                {
                    if ((crc & 32768) != 0)
                    {
                        crc = (ushort)((crc << 1) ^ generator);
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }
            data[data.Length - 1] = (byte)(crc % 256);
            data[data.Length - 2] = (byte)(crc / 256);
            return data;
        }

        private byte[] aesCrypt(byte[] message, byte[] key, byte[] iv)
        {
            AesManaged aes = new AesManaged();
            aes.Mode = CipherMode.CBC;
            aes.Key = key;
            if (iv != null)
                aes.IV = iv;
            aes.Padding = PaddingMode.PKCS7;
            var encryptor = aes.CreateEncryptor();
            return encryptor.TransformFinalBlock(message, 0, message.Length);
        }

        private byte[] aesDecrypt(byte[] message, byte[] key, byte[] iv)
        {
            AesManaged aes = new AesManaged();
            aes.Mode = CipherMode.CBC;
            aes.Key = key;
            aes.IV = iv;
            aes.Padding = PaddingMode.PKCS7;
            var dencryptor = aes.CreateDecryptor();
            return dencryptor.TransformFinalBlock(message, 0, message.Length);
        }

        private byte[] sha256(byte[] message)
        {
            SHA256Managed sha = new SHA256Managed();
            return sha.TransformFinalBlock(message, 0, message.Length);
        }

        private byte[] generateKey(byte[] macaddress)
        {
            string skey1 = "6F756A796D777134636C763933373879",
                   skey2 = "6231306A676664397937767375646139";
            byte[] key1 = skey1.ToHex(),
                   key2 = skey2.ToHex(),
                   key = new byte[16];
            for (int j = 0; j < 16; j++)
                key[j] = (byte)(((key1[j] << 4) ^ key2[j]) & 255);
            key = sha256(key);
            for (int j = 0; j < 12; j++)
                key[j] = (byte)((key[j] >> 6) ^ macaddress[j]);
            for (int j = 12; j < 16; j++)
                key[j] = (byte)((key[j] >> 6) ^ 48);
            return sha256(key);
        }

        private byte[] generateLinkKey(byte[] mac, byte[] basekey)
        {
            string skey4 = "52A4F5DB7FC0751F974A887E76E3696E07A4091571FB734DC4CB85A8CBB6DCC06F7C95C318978B7418F3AD51EA690A85";
            byte[] iv = skey4.Substring(0, 16).ToHex();
            var key = generateKey(mac);
            return aesDecrypt(basekey, key, iv);
        }

        private byte[] generateLinkKey(byte[] mac)
        {
            string skey4 = "52A4F5DB7FC0751F974A887E76E3696E07A4091571FB734DC4CB85A8CBB6DCC06F7C95C318978B7418F3AD51EA690A85";
            byte[] iv = skey4.Substring(0, 32).ToHex(),
                   en = skey4.Substring(32).ToHex();
            var key = generateKey(mac);
            return aesDecrypt(en, key, iv);
        }
    }
}

//build_message(1, 15,
//      1, 0,
//      3, 6, 68, 84, 73, 77, 68, 4,
//      4, 1, 2,
//      5, 0,
//      7, 17, 70, 70, 58, 70, 70, 58, 70, 70, 58, 70, 70, 58, 70, 70, 58, 70, 70, 67, 67,
//      9, 0);