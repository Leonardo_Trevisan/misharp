﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Linq;

using Plugin.BLE.Abstractions.Contracts;
using static System.Diagnostics.Trace;
using Plugin.BLE.Abstractions.EventArgs;
using Plugin.BLE.Abstractions.Extensions;
using Plugin.BLE.Abstractions.Utils;

namespace MiSharp.Model.Devices
{
    /// <summary>
    /// Implementação da comunicação com a MiBand3
    /// </summary>
    public class MiBand3 : Smartwatch
    {
        //Chave de bytes aleatórios gerados para autentificação
        private byte[] key;

        public override string DeviceName =>
            "Mi Band 3";

        /// <summary>
        /// Obtém característica de autentificação
        /// </summary>
        public ICharacteristic AuthService =>
            GetServiceByUuid("00000009-0000-3512-2118-0009af100700");

        /// <summary>
        /// Obtém característica de contagem de passos
        /// </summary>
        public ICharacteristic StepService =>
            GetServiceByUuid("00000007-0000-3512-2118-0009af100700");

        /// <summary>
        /// Obtém característica de bateria
        /// </summary>
        public ICharacteristic BateryService =>
            GetServiceByUuid("00000006-0000-3512-2118-0009af100700");

        /// <summary>
        /// Obtém característica de informações do usuário
        /// </summary>
        public ICharacteristic UserInfoService =>
            GetServiceByUuid("00000008-0000-3512-2118-0009af100700");

        /// <summary>
        /// Obtém característica de relógio interior da miband
        /// </summary>
        public ICharacteristic CurrentTimeService =>
            GetServiceByUuid("00002a2b-0000-1000-8000-00805f9b34fb");

        /// <summary>
        /// Obtém característica de controle da frequência cardiaca
        /// </summary>
        public ICharacteristic CurrentHeartRateControlService =>
            GetServiceByUuid("00002a39-0000-1000-8000-00805f9b34fb");

        /// <summary>
        /// Obtém característica da frequência cardiaca
        /// </summary>
        public ICharacteristic CurrentHeartRateService =>
            GetServiceByUuid("00002a37-0000-1000-8000-00805f9b34fb");

        /// <summary>
        /// Obtém característica de configuração
        /// </summary>
        public ICharacteristic ConfigurationService =>
            GetServiceByUuid("00000003-0000-3512-2118-0009af100700");

        public MiBand3()
        {
            //Gera uma chave aleatória para a autentificação.
            key = new byte[18];
            //Ela deve começar com 1 e 0
            key[0] = 1;
            key[1] = 0;
            //E depois pode ter qualquer sequência, no caso os valores
            //da função 7x + 3
            for (byte i = 2; i < key.Length; i++)
                key[i] = (byte)(7 * i + 3);
        }

        /// <summary>
        /// Lê os passos.
        /// </summary>
        public override async Task<int> GetSteps()
        {
            var step = StepService;
            if (step.CanRead)
            {
                byte[] data = await step.ReadAsync();
                //Converte os bytes de resposta em contagem de passos
                return data[1] + 256 * data[2] + 256 * 256 * data[3];
            }
            return -1;
        }

        /// <summary>
        /// Lê bateria.
        /// </summary>
        public override async Task<float> GetBatery()
        {
            var batery = BateryService;
            if (batery.CanRead)
            {
                byte[] data = await batery.ReadAsync();
                //Converte byte da bateria em porcentagem.
                return data[1] / 100f;
            }
            return -1;
        }

        //last = -2 indica que nenhum dado está sendo esperado
        //last = -1 indica que um dado de frequêcia cardiaca está sendo buscado
        //last > 0 indica uma frequência cardiaca
        int last = -2;
        /// <summary>
        /// Lê frequência cardiaca.
        /// </summary>
        public override async Task<int> GetHearthFrequency()
        {
            var heart = CurrentHeartRateService;
            if (heart.CanUpdate && last == -2)
            {
                last = -1;
                //Adiciona evento para executar quando
                //dados de frequência cardaiaca forem registrados
                heart.ValueUpdated += Heart_ValueUpdated;
                //Solicita atualização de dados de frequência cardíaca
                await heart.StartUpdatesAsync();
            }
            return last;
        }

        /// <summary>
        /// Ativa registro de frequência cardiaca de forma automática
        /// a cada intervalo de tempo (1 minuto como padrão).
        /// </summary>
        public async Task EnableRealTime(byte minutes = 1)
        {
            var control = CurrentHeartRateControlService;
            //20 é uma constante
            await control.WriteAsync(new byte[] { 20, minutes });
        }

        /// <summary>
        /// evento a ser executado e salvar a frequência cardiaca
        /// </summary>
        private void Heart_ValueUpdated(object sender, CharacteristicUpdatedEventArgs e)
        {
            last = e.Characteristic.Value[1];
        }

        /// <summary>
        /// Adiciona evento personalizado, execcutado quando um medição
        /// automática de frequência cardiaca é realizada
        /// </summary>
        private event Action<int> hearthmeseured;
        public event Action<int> OnHearthFrequencyMeasured
        {
            add
            {
                hearthmeseured += value;
                var heart = CurrentHeartRateService;
                heart.ValueUpdated += (obj, e) =>
                {
                    if (hearthmeseured != null)
                        hearthmeseured(e.Characteristic.Value[1]);
                };
            }
            remove
            {
                hearthmeseured -= value;
            }
        }

        /// <summary>
        /// Define tempo na MiBand
        /// </summary>
        public async void SetTime(DateTime datetime)
        {
            var time = CurrentTimeService;
            byte[] data = new byte[]
            {
                (byte)(datetime.Year / 256),
                (byte)(datetime.Year % 256),
                (byte)datetime.Month,
                (byte)datetime.Day,
                (byte)datetime.Hour,
                (byte)datetime.Minute,
                (byte)datetime.Second,
                (byte)((int)datetime.DayOfWeek == 7 ? 7 : (int)datetime.DayOfWeek)
            };
            if (time.CanWrite)
            {
                await time.WriteAsync(data);
            }
        }

        /// <summary>
        /// Autentifica na MiBand
        /// </summary>
        public override async Task Authentication()
        {
            var auth = AuthService;
            //Define o método a ser executado quando a autentificação
            //retorna dados
            auth.ValueUpdated += ValueUpdateAsync;
            await auth.StartUpdatesAsync();
            //Caso a autentificação já tenha sido realizada, ele pula a etapa
            //registrar a chave de autentificação.
            if (LocalInfo.Current["SessionStarted"] == "ok")
            {
                //Manda [2 0] para iniciar uma nova conexão
                await auth.WriteAsync(new byte[] { 2, 0 });
            }
            else
            {
                //Envia chave aleatória para registrar
                //uma nova chave de autentificação.
                await auth.WriteAsync(key);
            }
        }
        private async void ValueUpdateAsync(object sender, CharacteristicUpdatedEventArgs args)
        {
            var auth = AuthService;
            //WriteLine($"\t\t\t\t\t\tMi Band Value Updated");
            byte[] data = args.Characteristic.Value;
            //for (int i = 0; i < data.Length; i++)
            //    Write(data[i].ToString());
            ////Autentificação com Sucesso
            if (data[0] == 16 && data[2] == 1)
            {
                WriteLine($"\t\t\t\t\t\tMi Band output {data[1]}");
                //Chave registrada, iniciar autentificação
                if (data[1] == 1)
                {
                    await auth.WriteAsync(new byte[] { 2, 0 });
                }
                //Após receber uma chave aleatória (key desta classe) a
                //smart band retorna outra chave, precisamos realizar calculos
                //especificos para poder mandar uma password que libera a conexão
                //com a miband
                else if (data[1] == 2)
                {
                    //a password começa com [3 0] e usa a nossa chave aleatória
                    //junta da chave aleatória para gerar os outros 16 bytes a 
                    //partir de uma encriptação Aes
                    byte[] password = new byte[18];
                    password[0] = 3;
                    password[1] = 0;

                    byte[] sessionkey = new byte[16];
                    Array.Copy(key, 2, sessionkey, 0, 16);

                    byte[] buffer = new byte[16];
                    Array.Copy(data, 3, buffer, 0, 16);

                    AesManaged aes = new AesManaged();
                    aes.Mode = CipherMode.ECB;
                    aes.Key = sessionkey;
                    aes.Padding = PaddingMode.None;
                    var encryptor = aes.CreateEncryptor();
                    byte[] cryptcdata = encryptor.TransformFinalBlock(buffer, 0, buffer.Length);

                    for (int i = 0; i < cryptcdata.Length; i++)
                        password[i + 2] = cryptcdata[i];

                    await auth.WriteAsync(password);
                }
                //Autentificação finalizada
                else if (data[1] == 3)
                {
                    WriteLine("\t\t\t\t\t\tAutentication Succefful");
                    LocalInfo.Current["SessionStarted"] = "ok";
                    auth.ValueUpdated -= ValueUpdateAsync;
                    this.Authenticated = true;
                }
            }
        }
    }
}