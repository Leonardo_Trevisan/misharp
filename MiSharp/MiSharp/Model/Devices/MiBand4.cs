﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiSharp.Model.Devices
{
    /// <summary>
    /// A implementar, no caso, está mantendo os mesmos comportamentos
    /// da MiBand3, o que pode ser o suficiente.
    /// </summary>
    public class MiBand4 : MiBand3
    {
        public override string DeviceName => "Mi Smart Band 4";
    }
}