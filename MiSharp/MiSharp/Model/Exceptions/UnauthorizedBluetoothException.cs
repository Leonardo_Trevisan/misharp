﻿using System;

namespace MiSharp.Model.Exceptions
{
    using Language;
    public class UnauthorizedBluetoothException : Exception
    {
        public override string Message => Language.Current["ERRO0002"];
    }
}
