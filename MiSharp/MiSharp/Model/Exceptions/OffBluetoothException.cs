﻿using System;

namespace MiSharp.Model.Exceptions
{
    using Language;
    public class OffBluetoothException : Exception
    {
        public override string Message => Language.Current["ERRO0003"];
    }
}
