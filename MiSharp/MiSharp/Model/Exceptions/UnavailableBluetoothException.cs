﻿using System;

namespace MiSharp.Model.Exceptions
{
    using Language;
    public class UnavailableBluetoothException : Exception
    {
        public override string Message => Language.Current["ERROR001"];
    }
}
