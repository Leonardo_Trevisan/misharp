﻿using System;

namespace MiSharp.Model.Exceptions
{
    using Language;
    public class DeviceNotFoundException : Exception
    {
        public override string Message => Language.Current["ERRO0004"];
    }
}
