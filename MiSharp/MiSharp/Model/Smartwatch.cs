﻿using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;

namespace MiSharp.Model
{
    using Exceptions;
    using System.Threading.Tasks;

    /// <summary>
    /// Classe base que trás recursos de comunicação bluetooth com as
    /// smartbands.
    /// </summary>
    public abstract class Smartwatch
    {
        private IDevice device;
        private IAdapter adapter;
        private byte[] macaddress;
        private List<IDevice> list;
        private IBluetoothLE bluetoothBLE;
        internal List<ICharacteristic> services;

        /// <summary>
        /// Propriedade que indica se o smartband está conectada ao app.
        /// </summary>
        public bool Connected { get; protected set; } = false;
        /// <summary>
        /// Propriedade que indica se o smartband está autenticado ao app,
        /// caso exista autentificação.
        /// </summary>
        public bool Authenticated { get; protected set; } = false;
        /// <summary>
        /// Retorna os bytes do endereço MAC do smartband.
        /// </summary>
        public byte[] MacAddress => macaddress;

        public Smartwatch()
        {
            //Obtém adaptadores bluetooth
            this.bluetoothBLE = CrossBluetoothLE.Current;
            this.adapter = CrossBluetoothLE.Current.Adapter;
            this.list = new List<IDevice>();
            this.services = new List<ICharacteristic>();
        }

        /// <summary>
        /// Método que conecta com a smartbands e retira as características.
        /// </summary>
        public async Task Connect()
        {
            //Testa se o bluetooth está disponível, ligado e a permissão
            //do app para seu uso.
            if (bluetoothBLE.State == BluetoothState.Unavailable)
                throw new UnavailableBluetoothException();
            if (bluetoothBLE.State == BluetoothState.Unauthorized)
                throw new UnauthorizedBluetoothException();
            if (bluetoothBLE.State == BluetoothState.Off)
                throw new OffBluetoothException();

            list.Clear();
            adapter.ScanTimeout = 10000;
            adapter.ScanMode = ScanMode.Balanced;
            //Define um evento a ser executado quando um dispositivo for
            //encontrado.
            adapter.DeviceAdvertised += (obj, ea) =>
            {
                if (!list.Contains(ea.Device))
                    list.Add(ea.Device);
            };
            //Espera(pausa) função, retornando a quem chamou, enquanto
            //o scan ocorre a procura de dispositivos bluetooth.
            await adapter.StartScanningForDevicesAsync();

            //Caso nenhum device seja encontrado, retorna erro.
            if (list.Count == 0)
                throw new DeviceNotFoundException();

            //Encontra o device com o nome em DeviceName.
            foreach (IDevice dvc in list)
            {
                if (dvc.Name.Contains(DeviceName))
                {
                    device = dvc;
                    break;
                }
            }
            if (device == null)
                throw new DeviceNotFoundException();

            //Obtém o endereço MAC do dispositivo conectado
            string address =
                device.NativeDevice.GetType().GetRuntimeProperties()
                .FirstOrDefault(p => p.Name == "Address")?
                .GetValue(device.NativeDevice).ToString();
            //Obtém bytes do endereço MAC
            macaddress = address.Replace(":", "")
                .Select(c => (byte)c).ToArray();

            //Olha dados locais para indicar um nova seção com um dispositivo
            //ou, uma conexão com um novo dispositivo.
            if (LocalInfo.Current["deviceaddress"] == address)
            {
                LocalInfo.Current["SessionStarted"] = "ok";
            }
            else
            {
                LocalInfo.Current["SessionStarted"] = null;
                LocalInfo.Current["deviceaddress"] = address;
            }

            //Indica que a conexão foi completa
            Connected = true;

            //Para de procurar novos dispositivos
            await adapter.StopScanningForDevicesAsync();

            //Lê serviços
            try
            {
                await adapter.ConnectToDeviceAsync(device);

                services.Clear();
                foreach (var service in await device.GetServicesAsync())
                {
                    //Lê características de cada serviço
                    var characteristics = await service.GetCharacteristicsAsync();
                    foreach (var characteristic in characteristics)
                        this.services.Add(characteristic);
                }
            }
            catch (DeviceConnectionException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Retorna um serviço/característica com uma determinada Uuid.
        /// </summary>
        public ICharacteristic GetServiceByUuid(string Uuid)
            => services.Find(c => c.Uuid == Uuid);

        public List<ICharacteristic> GetAllServices() => services;

        /// <summary>
        /// Metodo que representa o nome exato da SmartBand
        /// </summary>
        public abstract string DeviceName { get; }

        /// <summary>
        /// Método abstrato que representa a autentificação com o aplicativo.
        /// Dispositivo básico em quase todas as smartbands.
        /// </summary>
        public abstract Task Authentication();

        /// <summary>
        /// Método genérico para a obtensão de quantidade de passos.
        /// </summary>
        public abstract Task<int> GetSteps();

        /// <summary>
        /// Método genérico para a obtensão de bateria.
        /// </summary>
        public abstract Task<float> GetBatery();

        /// <summary>
        /// Método genérico para a obtensão de frequência cardiaca.
        /// </summary>
        public abstract Task<int> GetHearthFrequency();
    }
}