﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MiSharp.Model
{
    /// <summary>
    /// Classe para facilitar persistências de dados simples.
    /// </summary>
    public class LocalInfo
    {
        public static LocalInfo Current = new LocalInfo();

        private Dictionary<string, string> info
            = new Dictionary<string, string>();
        private string filename;
        private LocalInfo()
        {
            filename = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder
                    .LocalApplicationData), "localinfo.text");
            if (!File.Exists(filename))
                File.Create(filename).Close();
            StreamReader sr = new StreamReader(filename);
            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                if (line.Length < 3)
                    continue;
                var codemess = line.Split(new char[] { ':' }, 2);
                info.Add(codemess[0], codemess[1]);
            }
            sr.Close();
        }

        public string this[string code]
        {
            get => info.ContainsKey(code) ? info[code] : null;
            set
            {
                if (!info.ContainsKey(code))
                {
                    StreamWriter sw = new StreamWriter(filename, true);
                    sw.WriteLine(code + ":" + value);
                    sw.Close();
                }
                info[code] = value;
            }
        }

        private readonly object obj = new object();
        public void Save()
        {
            lock (obj)
            {
                StreamReader reader = new StreamReader(filename);
                string[] lines = reader.ReadToEnd().Split(new char[] { '\n' },
                    StringSplitOptions.RemoveEmptyEntries);
                reader.Close();
                StreamWriter writer = new StreamWriter(filename, false);
                foreach (var key in info.Keys)
                    writer.WriteLine(key + ":" + info[key]);
                writer.Close();
            }
        }

        public void Reset()
        {
            File.Delete(filename);
            if (!File.Exists(filename))
                File.Create(filename).Close();
        }
    }
}