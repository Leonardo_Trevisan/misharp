﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Matcha.BackgroundService;

namespace MiSharp
{
    using Services;
    using Model.Devices;
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            //BackgroundAggregatorService.Add(
            //    () => new SmartwatchUpdater(new MiBand3()));
            //BackgroundAggregatorService.StartBackgroundService();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
