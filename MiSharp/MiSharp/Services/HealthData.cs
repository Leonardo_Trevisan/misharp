﻿using System;

namespace MiSharp.Services
{
    /// <summary>
    /// Representa um dado de saúde de um usuário qualquer.
    /// Já existe um exemplo de implementação de backend para
    /// enviar os dados.
    /// </summary>
    public class HealthData
    {
        private bool exist = false;
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public DateTime Moment { get; set; }
        public int Steps { get; set; }
        public int HearthFrequency { get; set; }
        public int Oximetry { get; set; }

        public static HealthData FromString(string data)
        {
            var arr = data.Split(new char[] { ':' },
                StringSplitOptions.RemoveEmptyEntries);
            HealthData result = new HealthData();
            result.ID = int.Parse(arr[0]);
            result.CustomerID = int.Parse(arr[1]);
            result.Moment = DateTime.Parse(arr[2]);
            result.Steps = int.Parse(arr[3]);
            result.HearthFrequency = int.Parse(arr[4]);
            result.Oximetry = int.Parse(arr[5]);
            return result;
        }

        public override string ToString() =>
            $"{this.ID}:{this.CustomerID}:{this.CustomerID}:{this.Moment.ToShortDateString()}:" +
            $"{this.Steps}:{this.HearthFrequency}:{this.Oximetry}";
    }
}