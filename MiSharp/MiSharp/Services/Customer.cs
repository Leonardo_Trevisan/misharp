﻿namespace MiSharp.Services
{
    /// <summary>
    /// Representa um usuário qualquer do serviço.
    /// Classe ainda deve se melhorada para ter mais campos
    /// e representar melhor o cliente.
    /// </summary>
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
