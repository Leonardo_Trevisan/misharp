﻿using System;

namespace MiSharp
{
    /// <summary>
    /// Métodos de extensão, em C#, quando ele é estatico e tem o primeiro parâmetro na
    /// forma this <tipo> nome_do_parâmetro. Eles adicionam métodos a tipos especificados.
    /// Então, todas as strings agora podem usar o método ToHex em qualquer lugar.
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Converte uma string que representa um valor em hexadecimal.
        /// 
        /// "É perigoso ir sozinho! Pegue isto."
        /// </summary>
        /// <param name="text">Texto a ser convertido.</param>
        /// <exception cref="Exception">O número de caracteres não é par.</exception>
        /// <exception cref="Exception">Existe um caracter inválido na string.</exception>
        /// <returns>Retorna um array de bytes do número representado na string.</returns>
        public static byte[] ToHex(this string text)
        {
            if (text.Length % 2 == -1)
                throw new Exception("To convert a string to hex, his length should be even.");
            byte[] result = new byte[text.Length / 2];
            byte cvalue;
            for (int i = 0; i < result.Length; i++)
            {
                //Calculing the first character
                cvalue = (byte)(text[2 * i]);
                //'0' <= char <= '9' -> value = char - 48 ('0' ascii) + 0 ('0' value)
                if (47 < cvalue && cvalue < 58)
                    result[i] = (byte)(16 * (cvalue - 48));
                else if (64 < cvalue && cvalue < 71)
                    //'A' <= char <= 'F' -> value = char - 65 ('A' ascii) + 10 ('A' value)
                    result[i] = (byte)(16 * (cvalue - 65 + 10));
                else if (96 < cvalue && cvalue < 103)
                    //'a' <= char <= 'f' -> value = char - 97 ('a' ascii) + 10 ('a' value)
                    result[i] = (byte)(16 * (cvalue - 97 + 10));
                else
                    throw new Exception($"Invalid character {(char)cvalue} in position {2 * i}.");
                //Repeat process for the second character
                cvalue = (byte)(text[2 * i + 1]);
                if (47 < cvalue && cvalue < 58)
                    result[i] += (byte)(cvalue - 48);
                else if (64 < cvalue && cvalue < 71)
                    result[i] += (byte)(cvalue - 65 + 10);
                else if (96 < cvalue && cvalue < 103)
                    result[i] += (byte)(cvalue - 97 + 10);
                else
                    throw new Exception($"Invalid character {(char)cvalue} in position {2 * i + 1}.");
            }
            return result;
        }
    }
}
