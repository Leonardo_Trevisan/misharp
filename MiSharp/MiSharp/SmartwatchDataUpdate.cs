﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

namespace MiSharp
{
    using Model;
    public class StartSmartwatchDataUpdateMessage { }

    public class StopSmartwatchDataUpdateMessage { }

    /// <summary>
    /// Aqui vem a implementação do comportamento contínuo com a aplicação em
    /// segundo plano. O método ReadAndUpdate é executado mesmo quando o aplicativo
    /// estiver fechado.
    /// </summary>
    public class SmartwatchDataUpdate
    {
        public static void Start()
        {
            var message = new StartSmartwatchDataUpdateMessage();
            MessagingCenter.Send(message, "StartSmartwatchDataUpdateMessage");
        }
        public static void Stop()
        {
            var message = new StopSmartwatchDataUpdateMessage();
            MessagingCenter.Send(message, "StopSmartwatchDataUpdateMessage");
        }

        /// <summary>
        /// Implementação assíncrona, ainda não implementada. Só tem a estrtutura
        /// preparada para futuramente, poder ler os dados da band mesmo com o
        /// aplicativo fechado.
        /// </summary>
        public async Task ReadAndUpdate()
        {
            for (int j = 0; j < 1000; j++)
            {
                LocalInfo.Current["variavel"] = j.ToString();
                LocalInfo.Current.Save();
                await Task.Delay(3000);
            }
            return;
        }
    }
}
