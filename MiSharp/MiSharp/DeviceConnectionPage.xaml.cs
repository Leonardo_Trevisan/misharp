﻿using System;
using System.ComponentModel;

using Xamarin.Forms;

namespace MiSharp
{
    using Model;
    using Model.Devices;

    /// <summary>
    /// Código associado a tela de conexão com smart bands.
    /// </summary>
    [DesignTimeVisible(true)]
    public partial class DeviceConnectionPage : ContentPage
    {
        public Smartwatch Smartwatch { get; set; } = null;

        public DeviceConnectionPage()
        {
            InitializeComponent();
        }

        private async void ConnectiWithMiBand3(object sender, EventArgs e)
        {
            bthn5.IsEnabled = btmi3.IsEnabled = btmi4.IsEnabled = false;
            var watch = new MiBand3();
            try
            {
                await watch.Connect();
                await watch.Authentication();
            }
            catch { }
            if (watch.Connected)
            {
                this.Smartwatch = watch;
                await this.DisplayAlert("Sucesso", "A conexão com o" +
                    " MiBand3 ocorreu com sucesso.", "Ok");
                await Navigation.PopAsync();
            }
            else
            {
                await this.DisplayAlert("Erro", "Não foi possível" +
                    "conectar com o MiBand3.", "Ok");
            }
            bthn5.IsEnabled = btmi3.IsEnabled = btmi4.IsEnabled = true;
        }

        private async void ConnectiWithMiBand4(object sender, EventArgs e)
        {
            bthn5.IsEnabled = btmi3.IsEnabled = btmi4.IsEnabled = false;
            var watch = new MiBand4();
            try
            {
                await watch.Connect();
                await watch.Authentication();
            }
            catch { }
            if (watch.Connected)
            {
                this.Smartwatch = watch;
                await this.DisplayAlert("Sucesso", "A conexão com o" +
                    " MiBand3 ocorreu com sucesso.", "Ok");
                await Navigation.PopAsync();
            }
            else
            {
                await this.DisplayAlert("Erro", "Não foi possível" +
                    "conectar com o MiBand3.", "Ok");
            }
            bthn5.IsEnabled = btmi3.IsEnabled = btmi4.IsEnabled = true;
        }

        private async void ConnectiWithHonorBand5(object sender, EventArgs e)
        {
            bthn5.IsEnabled = btmi3.IsEnabled = btmi4.IsEnabled = false;
            var watch = new HonorBand5();
            try
            {
                await watch.Connect();
                await watch.Authentication();
            }
            catch { }
            if (watch.Connected)
            {
                this.Smartwatch = watch;
                await this.DisplayAlert("Sucesso", "A conexão com o" +
                    " Honor Band 5 ocorreu com sucesso.", "Ok");
                await Navigation.PopAsync();
            }
            else
            {
                await this.DisplayAlert("Erro", "Não foi possível" +
                    "conectar com o Honor Band 5.", "Ok");
            }
            bthn5.IsEnabled = btmi3.IsEnabled = btmi4.IsEnabled = true;
        }
    }
}